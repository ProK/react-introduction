import React from "react";

function TKPText(props) {
  // Retrieves both parameters passed from App component
  const { name, passFunction } = props;

  // returns the whole div
  return (
    <div>
      <p> Greetings {name} </p>
      <button onClick={() => passFunction(name)}> Press Me </button>
    </div>
  );
}

export default TKPText;
