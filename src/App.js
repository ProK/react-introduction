import React from "react";
import TKPText from "./Components/Text";
import logo from "./logo.svg";
import "./App.css";

function App() {
  // Creates a function with receives the parameter name and prints it in the web console (F12 in browser)
  const myFunction = name => {
    console.log("Your name is " + name);
  };
  // Returs a simple web view, it uses our function from Text component and we pass it two parameters
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <TKPText name="TheKarinsProject" passFunction={myFunction} />
      </header>
    </div>
  );
}

export default App;
